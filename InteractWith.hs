import System.Environment (getArgs)

-- Reads the input file, applies the transf function to ints contents
-- And then writes that to the output file
-- Some sort of a persistent map of a file to another one.
interactWith inputFile transf outputFile = do
  contents <- readFile inputFile
  writeFile outputFile (transf contents)

main = mainWith myFunction
  where mainWith f = do
          args <- getArgs
          case args of
            [i, o] -> interactWith i f o
            _ -> putStrLn "use like so: binaryName inputFilePath outputFilePath"
        
        myFunction = id
